# SPDX-FileCopyrightText: 2020,2022,2024 Jens Lechtenbörger
# SPDX-License-Identifier: CC0-1.0

ORGS :=
SOCKET_ID :=

.PHONY: all build build-multiplex clean tts

all: clean build zip printed-pdfs

build:
	emacs --batch --load elisp/publish.el

devel:
	emacs --batch --eval '(setq devel t)' --load elisp/publish.el

build-multiplex: multiplex
	emacs --batch --load elisp/publish.el

clean:
	rm -f *.aux *.bbl *.fls *.html *latexmk *.tex *.tex~ *.pdf
	rm -rf public
	rm -rf ~/.org-timestamps

cache:
	rm -rf old-artifacts
	curl -L https://gitlab.com/oer/oer-courses/it-systems/-/jobs/artifacts/main/download?job=pages -o artifacts.zip
	unzip -n artifacts.zip -d old-artifacts
	cp -r old-artifacts/public/.org-timestamps ~
	cp -r old-artifacts/public .

zip:
	apt install zip
	rm -rf public/eval
	rm -f os*.zip
	zip -r os.zip public -x \*.pdf \*.mp4
	find public -name "*.pdf" -print | zip os-pdf.zip -@

printed-pdfs:
	rm -rf tmp/
	rm OS-printed-pdfs.zip
	mkdir -p tmp
	curl -L https://gitlab.com/oer/oer-courses/it-systems/-/jobs/artifacts/main/download?job=pages -o tmp/os-artifacts.zip
	apt install zip
	unzip -d tmp tmp/os-artifacts.zip
	cd tmp && find public/pdfs -name "*.pdf" -print | zip ../OS-printed-pdfs.zip -@

multiplex:
	$(foreach org,$(ORGS),./bin/create_multiplex.sh $(org) $(SOCKET_ID);)

tts:
	python /tts/tts.py public/tts/ public/audio/
