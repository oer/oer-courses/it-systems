;;; publish.el --- Publish reveal.js presentations from Org files
;; -*- Mode: Emacs-Lisp -*-
;; -*- coding: utf-8 -*-

;; SPDX-FileCopyrightText: 2019-2025 Jens Lechtenbörger
;; SPDX-License-Identifier: GPL-3.0-or-later

;;; License: GPL-3.0-or-later

;;; Commentary:
;; Publication of Org source files to reveal.js uses Org export
;; functionality offered by org-re-reveal and oer-reveal.
;; Initialization code for both is provided by emacs-reveal.
;; Note that org-re-reveal and oer-reveal are available on MELPA.
;;
;; Use this file from its parent directory with the following shell
;; command:
;; emacs --batch --load elisp/publish.el

;;; Code:
;; Avoid update of emacs-reveal, enable stacktraces, disable delays,
;; make an index, enable Graphviz/dot
(setq emacs-reveal-managed-install-p nil
      debug-on-error t
      oer-reveal-warning-delay nil
      oer-reveal-publish-makeindex t
      oer-reveal-publish-babel-languages '((dot . t) (emacs-lisp . t))
      oer-reveal-debug-cache t)

;; Create directory for Graphviz/dot
(make-directory "generated-img" t)

;; Copy ostriches
(copy-directory "figures/OS/ostriches" "figures.for-export/OS/ostriches" nil t)

;; Use citation link format of Org mode 9.5.
(setq emacs-reveal-cite-pkg 'org-re-reveal-citeproc)

;; Development settings for TTS
(defcustom org-re-reveal-tts-normalize-table
  '(("[ \t][ \t]+" " ") ; Replace horizontal whitespace.
    ("[ \t]+\n" "\n")   ; Remove trailing whitespace.
    ("’" "'")           ; Replace curly apostrophe.
    ;; If a space precedes a break element, replace with newline:
    ("[ ]+\\(<break time=[^>]+>\\)" "\n\\1")
    ;; If something else precedes a break element, keep it:
    ("\\([^\n]\\)\\(<break time=[^>]+>\\)" "\\1\n\\2")
    ;; Similarly for (non-) space following break elements:
    ("\\(<break time=[^>]+>\\)[ ]+" "\\1\n")
    ("\\(<break time=[^>]+>\\)\\([^\n]\\)" "\\1\n\\2")
    ;; Specific colons
    ("\\babout:\\([a-z]+\\)\\b" "about \\1")
    ;; Replace colon with dot
    (":" ".")
    ;; Replace some functions and math symbols.
    ("\\b$n$\\b" "ann")
    ("\\\\(n\\\\)" "ann")
    ("\\b$k$\\b" "kay")
    ("\\\\(k\\\\)" "kay")
    ("\\b$f_0$\\b" "f zero")
    ("\\b$f_1$\\b" "f one")
    ("\\b$f_2$\\b" "f two")
    ("\\b$f_3$\\b" "f three")
    ("\\b$2^n$\\b" "two to the ann")
    ("\\b$2^{n-1}$\\b" "two to the ann minus one")
    ("\\b$?2^{\\([0-9]+\\)}$?" "two to the \\1")
    (" = " " equals ")
    ("[ \n]-\\([1-9a-z]\\)\\b" " minus \\1")
    ("\\b\\([0-9]+\\)%" "\\1 percent")
    ;; Replace plural, but not as break time.
    ("\\([^\"]\\)1s\\b" "\\1ones")
    ;; Replace versions.
    ("\\b1[.]0\\b" "one dot oh")
    ("\\b4[.]x\\b" "four dot ex")
    ;; Remove parentheses around single digits.
    ("(\\([1-9]\\))" "\\1")
    ;; Replace decimal numbers.
    ("\\b\\([0-9]+\\)[.]\\([0-9]\\)\\b" "\\1 point \\2")
    ;; Replace hex numbers.
    ("\\b0x\\([1-9][0-9]\\)\\b" "hex \\1")
    ;; Guess years in range 1940-1999.
    ("\\b19\\([4-9][0-9]\\)" "nineteen \\1")
    ;; Guess years in range 2010-2029.
    ("\\b20\\([1-2][0-9]\\)" "twenty \\1")
    ;; Replace some numbers; use word-boundary matching here
    ;; (which also matches 1-bit etc.).
    ("\\b0s\\b" "zeros")
    ("\\b$?20$?\\b" "twenty")
    ("\\b$?30$?\\b" "thirty")
    ("\\b$?40$?\\b" "fourty")
    ("\\b$?50$?\\b" "fifty")
    ("\\b$?60$?\\b" "sixty")
    ("\\b$?70$?\\b" "seventy")
    ("\\b$?80$?\\b" "eighty")
    ("\\b$?90$?\\b" "ninety")
    ("\\b$?2\\([1-9]\\)$?\\b" "twenty——\\1") ;; remove —— below
    ("\\b$?3\\([1-9]\\)$?\\b" "thirty——\\1")
    ("\\b$?4\\([1-9]\\)$?\\b" "fourty——\\1")
    ("\\b$?5\\([1-9]\\)$?\\b" "fifty——\\1")
    ("\\b$?6\\([1-9]\\)$?\\b" "sixty——\\1")
    ("\\b$?7\\([1-9]\\)$?\\b" "seventy——\\1")
    ("\\b$?8\\([1-9]\\)$?\\b" "eighty——\\1")
    ("\\b$?9\\([1-9]\\)$?\\b" "ninety——\\1")
    ("\\b$?0$?\\b" "zero")
    ("\\b$?1$?\\b" "one")
    ("\\b$?2$?\\b" "two")
    ("\\b$?3$?\\b" "three")
    ("\\b$?4$?\\b" "four")
    ("\\b$?5$?\\b" "five")
    ("\\b$?6$?\\b" "six")
    ("\\b$?7$?\\b" "seven")
    ("\\b$?8$?\\b" "eight")
    ("\\b$?9$?\\b" "nine")
    ("——\\|()" "")
    ;; More numbers
    ("\\b$?10$?\\b" "ten")
    ("\\b$?11$?\\b" "eleven")
    ("\\b$?12$?\\b" "twelve")
    ("\\b$?13$?\\b" "thirteen")
    ("\\b$?14$?\\b" "fourteen")
    ("\\b$?15$?\\b" "fifteen")
    ("\\b$?16$?\\b" "sixteen")
    ("\\b$?17$?\\b" "seventeen")
    ("\\b$?18$?\\b" "eighteen")
    ("\\b$?19$?\\b" "nineteen")
    ("\\b$?100$?\\b" "one hundred")
    ("\\b$?128$?\\b" "one hundred and twentyeight")
    ("\\b$?256$?\\b" "two hundred and fiftysix")
    ("\\b$?365$?\\b" "threehundred and sixtyfive")
    ("\\b$?512$?\\b" "five hundred and twelve")
    ("\\b$?1000$?\\b" "one thousand")
    ("\\b$?1024$?\\b" "one thousand and twentyfour")
    ("\\b$?8080$?\\b" "eighty eighty")
    ("\\b$?8192$?\\b" "eight thousand onehundred and ninety two")
    ("\\b$?16384$?\\b" "sixteenthousand threehundred and eightyfour")
    ("\\b$?24576$?\\b" "twentyfourthousand fivehundred and seventysix")
    ("\\b$?32768$?\\b" "thirtytwothousand sevenhundred and sixtyeight")
    ("Q1" "queue one")
    ("Q2" "queue two")
    ("Q3" "queue three")
    ("Q4" "queue four")
    ;; Replace selected abbreviations.
    ("Nand2Tetris" "Nand to Tetris")
    ("reveal.js" "reveal jay ess")
    ("\\bSSML\\b" "ess ess em el")
    ("\\bTTS\\b" "tea tea ess")
    ("\\bALU\\b" "ay al you")
    ("\\bB\\b" "bytes")
    ("\\bNAPI\\b" "ann API")
    ("\\bAPI" "ay pea eye") ; no word boundary at end for plural
    ("\\bCFS\\b" "see eff ess")
    ("chmod" "change mod")
    ("\\bCPU\\b" "see pea you")
    ("\\bCPUs\\b" "see pea use")
    ("\\bGPU\\b" "djee pea you")
    ("\\bGPUs\\b" "djee pea use")
    ("\\bCRI\\b" "see are eye")
    ("\\bCS\\b" "see ess")
    ("\\bCSs\\b" "critical sections")
    ("\\bDNF\\b" "dee n f")
    ("\\bDNS\\b" "dee an ess")
    ("\\bDMux\\b" "deemux")
    ("\\bDSL\\b" "dee ess el")
    ("\\bDS" "distributed system")
    ("\\bFIFO\\b" "first in, first out,")
    ("\\bHDL\\b" "age dee el")
    ("\\bHDD" "age dee dee")
    ("\\bHDFS\\b" "age dee eff ess")
    ("\\bHTML\\b" "age tea em el")
    ("\\bHTTP\\b" "age tea tea pea")
    ("\\bID\\b" "eye dee")
    ("\\bIDs\\b" "eye dees")
    ("\\bIoT\\b" "eye oh tea")
    ("\\bIPv4\\b" "eye pea version four")
    ("\\bIPv6\\b" "eye pea version six")
    ("\\b\\([A-Z]\\)aaS\\b" "\\1——as a service")
    ("\\bI——" "infrastructure ")
    ("\\bP——" "platform ")
    ("\\bS——" "software ")
    ("\\bX——" "anything ")
    ("\\bIP\\b" "eye pea")
    ("\\bIT\\b" "eye tea")
    ("\\bIDTR\\b" "eye dee tea are")
    ("\\bIDT\\b" "eye dee tea")
    ("\\bKB\\b" "kilobytes")
    ("\\bKiB\\b" "keebeebytes")
    ("\\bLRU\\b" "el are you")
    ("\\bMB\\b" "megabytes")
    ("\\bMiB\\b" "mebeebytes")
    ("\\bGB\\b" "gigabytes")
    ("\\bGiB\\b" "gibeebytes")
    ("K8s" "kates")
    ("\\bL4\\b" "el four")
    ("\\bls\\b" "el ess")
    ("\\bMMU\\b" "em em you")
    ("\\bMX\\b" "mutual exclusion")
    ("Mac OS" "Mackowess")
    ("\\bOS" "operating system")
    ("passwd" "password")
    ("\\bPC" "pea sea")
    ("\\bps\\b" "pea ess")
    ("\\bQUIC\\b" "quick")
    ("\\bSQL" "sequel")
    ("\\bSSD" "ess ess dee")
    ("\\bssh" "ess ess age")
    ("\\bTCB" "tea sea bee")
    ("\\bTCP" "tea sea pea")
    ("\\bUDP" "you dee pea")
    ("stderr" "standard error")
    ("stdin" "standard input")
    ("stdout" "standard output")
    ("SELinux" "security enhanced linux")
    ("\\bTLB" "tea el bee")
    ("\\bURL" "you are al")
    ("\\bUS\\b" "you ess")
    ("\\bVM\\b" "vee emm")
    ("\\bVMs\\b" "vee emms")
    ("\\bVMM" "vee emm emm")
    ("\\bWi-?Fi" "why fy")
    ("RAM16K" "RAM sixteen kay")
    ("e[.]g[.]" "for example")
    ("E[.]g[.]" "For example")
    ("etc[.]" "etcetera.")
    ("i[.]e[.]" "that is")
    ("I[.]e[.]" "That is")
    ("I/O" "eye oh")
    ("input/output" "input output")
    ("producer/consumer" "producer consumer")
    ;; Hyphenated words do not work well.
    ;; Further below, a rule replaces a hyphen between characters with a space.
    ;; Thus, list hypenated words here that require special replacements or that
    ;; sound well without space.
    ("A-register" "ay register")
    ("A-instruction" "ay instruction")
    ("C-instruction" "sea instruction")
    ("D-register" "dee register")
    ("built-in" "builtin")
    ("break-through" "breakthrough")
    ("co-founder" "cofounder")
    ("counter-" "counter")
    ("\\b[dD]e-" "de")
    ("fan-in" "fan in")
    ("full-fledged" "fullfledged")
    ("large-scale" "largescale")
    ("re-insert" "reinsert")
    ("up-front" "upfront")
    ("eta-[cC]ognitive" "etacognitive")
    ("\\([fF]\\)ixed-" "\\1ixed")
    ("non-" "non")
    ("self-" "self")
    ("inter-" "inter")
    ("real-" "real")
    ("[rR]ead-[oO]nly" "reed only")
    ("[rR]ipple-[cC]arry" "ripplecarry")
    ("trade-off" "tradeoff")
    ("\\ba-bit\\b" "ay bit")
    ("-mapped" "mapped")
    ("warm-up" "warmup")
    ("-wise" "wise")
    ;; Pronounciation helpers
    ("(a)" "ay")
    ("(b)" "bee")
    ("(c)" "see")
    ("~r~" "are")
    ("~w~" "doubleyou")
    ("~x~" "ex")
    ("\\bx-" "ex ")
    ("\\by-" "why ")
    ("\\bM\\b" "em")
    ("\\b[xX]\\b" "ex")
    ("\\b[yY]\\b" "why")
    ("buses" "busses")
    ("cAdvisor" "sea advisor")
    ("\\betcd\\b" "etseedee")
    ("lease read" "lease reed")
    ("readable" "reedable")
    ("\\bmaybe read" "maybe reed")
    ("\\bbe read" "be rad")
    ("\\bindices" "indicees")
    ("put\\(s?\\) a\\b" "put\\1 ay") ; input/output(s)
    ("GitLab" "git lab")
    ("gigabit" "giguhbit")
    ("gigabyte" "giguhbyte")
    ("gigahertz" "giguhhurts")
    ("\\bknote" "kaynote")
    ("kubectl" "cube cuttel")
    ("kubelet" "cube lett")
    ("nanosecond" "nahno second")
    ("nginx" "engine ex")
    ("[Pp]lugin" "pluggin")
    ("Python" "piethon")
    ("timeout" "time out")
    ("YAML" "jamell")
    ;; Code
    ("System[.]\\([a-z]+\\)" "system dot \\1")
    ;; Domain names
    ("[.]de\\b" " dot dee ee")
    ;; Replace some symbols
    ("GNU/" "Gnu ")
    ("/\\([a-zA-Z]\\)" " slash \\1") ;
    ("“\\|”\\|„" "\"")
    (" [+] " " plus ")
    ("\\([a-zA-Z]\\)-\\([a-zA-Z]\\)" "\\1 \\2") ;; replace hyphen with space
    (" - " " minus ")
    (" [*] " " times ")
    ;; Remove parentheses at start and end of lines
    ("^(\\|)$" ""))
    "Normalization table understood by `iso-translate-conventions'.
Normalization supports correct pronounciation of words, numbers,
symbols, or abbreviations by TTS models.
For that purpose, this table contains a list of 2-element lists.
Both elements are regular expressions, where occurrences of the first
one are replaced by the second one when creating the input texts for
TTS processing.  This does not affect notes shown on slides.

Selected replacements:
- Replace several whitespaces with one.
- Avoid some UTF symbols.
- Make sure that SSML break elements appear on lines of their own.
- Replace colon with dot.
- Replace some math symbols.
- Replace digits, some numbers, some years.
- Replace some abbreviations.
- Replace some hyphenations.  By default, a rule replaces a hyphen between
  characters with a space.  When this does not sound well, a special rule
  is necessary.
- Selected pronounciation helpers for speaker CLB."
  :group 'org-export-re-reveal
  :type '(repeat (list string string))
  :package-version '(org-re-reveal . "3.33.0"))

;; Set up load-path.
(let ((install-dir
       (mapconcat #'file-name-as-directory
                  `(,user-emacs-directory "elpa" "emacs-reveal") "")))
  (add-to-list 'load-path install-dir)
  (condition-case nil
      ;; Either require package with above hard-coded location
      ;; (e.g., in docker) ...
      (require 'emacs-reveal)
    (error
     ;; ... or look for sibling "emacs-reveal".
     (add-to-list
      'load-path
      (expand-file-name "../../emacs-reveal/" (file-name-directory load-file-name)))
     (require 'emacs-reveal))))

;; Use local mathjax for offline use.
(setq org-re-reveal-mathjax-url "")
(add-to-list 'oer-reveal-plugins "mathjax")
(setq oer-reveal-plugin-4-config
      (concat oer-reveal-plugin-4-config "\nmathjax RevealMath.MathJax2 plugin/math/math.js"))

;; Publish reveal.js presentations.
(if (boundp 'devel)
    ;; For faster development cycle.  Use --eval '(setq devel t)'
    (let ((org-confirm-babel-evaluate oer-reveal-publish-confirm-evaluate)
          (org-publish-project-alist
           (list
            (list "org-presentations"
                  :base-directory "."
		  :makeindex oer-reveal-publish-makeindex
                  :include '("11-OS-Interrupts-II.org")
                  :exclude ".*"
                  :publishing-function 'oer-reveal-publish-to-reveal-and-pdf
                  :publishing-directory "./public")
            (list "index-terms"
	  :base-directory "."
	  ;; A file theindex.org (which includes theindex.inc) is created
          ;; due to assignment to oer-reveal-publish-makeindex above.
          ;; That setting defines the makeindex property of
          ;; org-presentations.  Then, theindex.org is automatically
          ;; published (from org-publish-projects with org-publish-file),
          ;; which is useless (and avoidable with oer-reveal-skip-theindex).
	  ;; Thus, publish as HTML here.  For this to work, index-terms.org
	  ;; is a manually created file including theindex.inc.
	  ;; The preparation and completion functions below set up an
	  ;; advice on org-html-link to rewrite links into presentations
	  ;; using org-re-reveal's anchor format.
	  :include '("index-terms.org")
	  :exclude ".*"
	  :preparation-function 'oer-reveal--add-advice-link
	  :completion-function 'oer-reveal--remove-advice-link
	  :publishing-function '(org-html-publish-to-html)
	  :publishing-directory "./public"))))
      (org-publish-all))
  ;; Normal publication.
  (oer-reveal-publish-all
   (list
    (list "texts"
       	  :base-directory "texts"
       	  :base-extension "org"
	  :exclude "config\\|license-template"
          :html-postamble ""
       	  :publishing-function '(oer-reveal-publish-to-html-and-pdf)
       	  :publishing-directory "./public/texts")
    (list "orgs"
       	  :base-directory "org-resources"
       	  :base-extension "org"
       	  :publishing-function 'org-publish-attachment
       	  :publishing-directory "./public/org-resources")
    (list "index-terms"
	  :base-directory "."
	  ;; A file theindex.org (which includes theindex.inc) is created
          ;; due to assignment to oer-reveal-publish-makeindex above.
          ;; That setting defines the makeindex property of
          ;; org-presentations.  Then, theindex.org is automatically
          ;; published (from org-publish-projects with org-publish-file),
          ;; which is useless (and avoidable with oer-reveal-skip-theindex).
	  ;; Thus, publish as HTML here.  For this to work, index-terms.org
	  ;; is a manually created file including theindex.inc.
	  ;; The preparation and completion functions below set up an
	  ;; advice on org-html-link to rewrite links into presentations
	  ;; using org-re-reveal's anchor format.
	  :include '("index-terms.org")
	  :exclude ".*"
	  :preparation-function 'oer-reveal--add-advice-link
	  :completion-function 'oer-reveal--remove-advice-link
	  :publishing-function '(org-html-publish-to-html)
	  :publishing-directory "./public")
    (list "generated-images"
	  :base-directory "generated-img"
	  :base-extension (regexp-opt '("png" "jpg" "ico" "svg" "gif"))
	  :publishing-directory "./public/generated-img"
	  :publishing-function 'org-publish-attachment
	  :recursive t)
    (list "images"
	  :base-directory "img"
	  :base-extension (regexp-opt '("png" "jpg" "ico" "svg" "gif"))
	  :publishing-directory "./public/img"
	  :publishing-function 'org-publish-attachment
	  :recursive t)
    (list "videos"
	  :base-directory "videos"
	  :base-extension (regexp-opt '("mp4"))
	  :publishing-directory "./public/videos"
	  :publishing-function 'org-publish-attachment
	  :recursive t)
    (list "title-logos"
	  :base-directory "non-free-logos/title-slide"
	  :base-extension (regexp-opt '("png" "jpg" "ico" "svg" "gif"))
	  :publishing-directory "./public/title-slide"
	  :publishing-function 'org-publish-attachment)
    (list "theme-logos"
	  :base-directory "non-free-logos/reveal-css"
	  :base-extension (regexp-opt '("png" "jpg" "ico" "svg" "gif"))
	  :publishing-directory "./public/reveal.js/css/theme"
	  :publishing-function 'org-publish-attachment)
    (list "highlightjs-css"
	  :base-directory "highlightjs-styles"
	  :base-extension "css"
	  :publishing-directory "./public/reveal.js/plugin/highlight"
	  :publishing-function 'org-publish-attachment)
    )))
