/**
 * SPDX-FileCopyrightText: 2024 Jens Lechtenbörger
 * SPDX-License-Identifier: CC-BY-SA-4.0
**/

quizVirtualization = {
    "info": {
        "name":    "", // Should be empty with emacs-reveal
        "main":    "Memorizing virtualization",
        "level1":  "Excellent!",          // 80-100%
        "level2":  "Please re-try.",      // 60-79%
        "level3":  "Please re-try.",      // 40-59%
        "level4":  "Maybe ask for help?", // 20-39%
        "level5":  "Please ask for help." //  0-19%, no comma here
    },
    "questions": [
	{
            "q": "Select correct statements about virtualization.",
            "a": [
                {"option": "Virtualization can refer to various concepts such memory, networking, or hardware in general.", "correct": true},
                {"option": "In our context, VMs make use of virtualized hardware.", "correct": true},
                {"option": "Guest operating systems can directly interact with the underlying hardware resources.", "correct": false},
                {"option": "Hardware virtualization can improve resources utilization.", "correct": true}
            ],
            "correct": "<p><span>Correct!</span></p>",
            "incorrect": "<p><span>No. (Hint: 3 statements are correct.)</span></p>" // no comma here
        },
        {
            "q": "Select correct statements related to virtualization.",
            "a": [
                {"option": "Virtualization allows running multiple instances of multiple OSs per physical server.", "correct": true},
                {"option": "Docker is a prominent example for a VMM.", "correct": false},
                {"option": "A VMM provides execution environments for guest OSs.", "correct": true},
                {"option": "A VMM intercepts hardware accesses by guest OSs, introducing overhead costs.", "correct": true}
            ],
            "correct": "<p><span>Correct!</span></p>",
            "incorrect": "<p><span>No. (Hint: 3 statements are correct.)</span></p>" // no comma here
        }
    ]
};
