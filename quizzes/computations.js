/**
 * SPDX-FileCopyrightText: 2024 Jens Lechtenbörger
 * SPDX-License-Identifier: CC-BY-SA-4.0
**/

quizComputations = {
    "info": {
        "name":    "", // Should be empty with emacs-reveal
        "main":    "What's running?",
        "level1":  "Excellent!",          // 80-100%
        "level2":  "Please re-try.",      // 60-79%
        "level3":  "Please re-try.",      // 40-59%
        "level4":  "Maybe ask for help?", // 20-39%
        "level5":  "Please ask for help." //  0-19%, no comma here
    },
    "questions": [
	{
            "q": "Select correct statements about computations.",
            "a": [
                {"option": "Running programs are managed as processes by the OS.", "correct": true},
                {"option": "To create a process, system calls are necessary.", "correct": true},
                {"option": "Not every process needs to contain a thread.", "correct": false},
                {"option": "The OS parallelizes computations with additional threads to keep all CPU cores busy.", "correct": false},
                {"option": "Hack supports multitasking.", "correct": false}
            ],
            "correct": "<p><span>Correct!</span></p>",
            "incorrect": "<p><span>No. (Hint: 2 statements are correct.)</span> Maybe check out <a href=\"10-OS-Introduction.html#slide-computations-processes-threads\">earlier slides</a>?</p>" // no comma here
        },
        {
            "q": "Select correct statements about system calls",
            "a": [
                {"option": "System calls protect resources and hardware access.", "correct": true},
                {"option": "The OS kernel creates a new thread for each system call.", "correct": false},
                {"option": "Retrieving the currently pressed key requires a system call.", "correct": true},
                {"option": "Printing text in some high-level language invokes system calls in the background to draw pixels on screen.", "correct": true},
                {"option": "In Hack, every program runs in kernel mode.", "correct": true}
            ],
            "correct": "<p><span>Correct!</span></p>",
            "incorrect": "<p><span>No. (Hint: Most statements are correct.)</span> Maybe check out <a href=\"10-OS-Introduction.html#slide-kernel\">earlier slides</a>?</p>" // no comma here
        }
    ]
};
