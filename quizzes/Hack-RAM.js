/**
 * SPDX-FileCopyrightText: 2024 Jens Lechtenbörger
 * SPDX-License-Identifier: CC-BY-SA-4.0
**/

quizHackRAM = {
    "info": {
        "name":    "", // Should be empty with emacs-reveal
        "main":    "Memory in Hack and beyond",
        "level1":  "Excellent!",          // 80-100%
        "level2":  "Please re-try.",      // 60-79%
        "level3":  "Please re-try.",      // 40-59%
        "level4":  "Maybe ask for help?", // 20-39%
        "level5":  "Please ask for help." //  0-19%, no comma here
    },
    "questions": [
	{
            "q": "Select correct statements about RAM in Hack.",
            "a": [
                {"option": "RAM consists of registers.", "correct": true},
                {"option": "Each register stores words of 14 bits.", "correct": false},
                {"option": "Each register stores words of 2 bytes.", "correct": true},
                {"option": "The <code>address</code> input of a RAM chip enumerates its registers.", "correct": true},
                {"option": "Some C-instructions embed RAM addresses.", "correct": false}
            ],
            "correct": "<p><span>Correct!</span></p>",
            "incorrect": "<p><span>No. (Hint: 3 statements are correct.)</span> Please revisit <a href=\"./05-Hack-Memory.html\">Hack Memory</a>.</p>" // no comma here
        },
        {
            "q": "Select correct statements related to RAM16K",
            "a": [
                {"option": "RAM16K stores 32 KiB.", "correct": true},
                {"option": "2^14 = 16384", "correct": true},
                {"option": "2^16 = 16384", "correct": false},
                {"option": "The <code>address</code> input is 14 bits wide.", "correct": true},
                {"option": "RAM of the Hack computer is limited to a single RAM16K.", "correct": true}
            ],
            "correct": "<p><span>Correct!</span></p>",
            "incorrect": "<p><span>No. (Hint: Most statements are correct.)</span> Please revisit <a href=\"./05-Hack-Memory.html\">Hack Memory</a>.</p>" // no comma here
        },
        {
            "q": "Select correct statements about real memory",
            "a": [
                {"option": "One byte consists of 8 bits.", "correct": true},
                {"option": "Real-world RAM (outside Hack) is byte-addressed.", "correct": true},
                {"option": "Sizes related to RAM are indicated with binary units such as KiB and GiB.", "correct": true},
                {"option": "RAM with a size of 1 GiB needs 30 address bits.", "correct": true}
            ],
            "correct": "<p><span>Correct!</span></p>",
            "incorrect": "<p><span>No. (Hint: All statements are correct.)</span> Please revisit <a href=\"05-Hack-memory.html\">Hack Memory</a>.</p>" // no comma here
        }
    ]
};
