/**
 * SPDX-FileCopyrightText: 2024 Jens Lechtenbörger
 * SPDX-License-Identifier: CC-BY-SA-4.0
**/

quizOverview = {
    "info": {
        "name":    "", // Should be empty with emacs-reveal
        "main":    "Did you get an overview?",
        "level1":  "Excellent!",          // 80-100%
        "level2":  "Please re-try.",      // 60-79%
        "level3":  "Please re-try.",      // 40-59%
        "level4":  "Maybe ask for help?", // 20-39%
        "level5":  "Please ask for help." //  0-19%, no comma here
    },
    "questions": [
	{
            "q": "Select correct statements about OS concepts (1/3).",
            "a": [
                {"option": "The main part of an OS is called core.", "correct": false},
                {"option": "The main part of an OS is called kernel.", "correct": true},
                {"option": "Applications use special registers or memory locations to interact with hardware.", "correct": false},
                {"option": "Application code invokes system calls to interact with hardware.", "correct": true},
                {"option": "Hardware devices can trigger interrupts to indicate events that should be handled by the OS.", "correct": true}
            ],
            "correct": "<p><span>Correct!</span></p>",
            "incorrect": "<p><span>No. (Hint: 3 statements are correct.)</span> Please revisit the <a href=\"#slide-os-plan\">overview</a>.</p>" // no comma here
        },
        {
            "q": "Select correct statements about OS concepts (2/3).",
            "a": [
                {"option": "The OS manages the execution of applications in terms of threads.", "correct": true},
                {"option": "The OS creates a new thread for each system call.", "correct": false},
                {"option": "The OS schedules threads for execution on CPU cores.", "correct": true},
                {"option": "The OS creates new threads to make use of all CPU cores.", "correct": false},
                {"option": "Time-slicing creates an illusion of parallelism on single CPU cores.", "correct": true},
                {"option": "The OS locks shared resources to avoid update anomalies.", "correct": false}
            ],
            "correct": "<p><span>Correct!</span></p>",
            "incorrect": "<p><span>No. (Hint: 3 statements are correct.)</span> Please revisit the <a href=\"#slide-os-plan\">overview</a>.</p>" // no comma here
        },
        {
            "q": "Select correct statements about OS concepts (3/3).",
            "a": [
                {"option": "Kernel code is stored in ROM.", "correct": false},
                {"option": "Applications can access more RAM if they need it.", "correct": false},
                {"option": "The OS manages applications as processes.", "correct": true},
                {"option": "The OS allocates RAM as part of virtual memory.", "correct": true},
                {"option": "Threads of a process share resources.", "correct": true},
                {"option": "The OS protects files with encryption.", "correct": false}
            ],
            "correct": "<p><span>Correct!</span></p>",
            "incorrect": "<p><span>No. (Hint: 3 statements are correct.)</span> Please revisit the <a href=\"#slide-os-plan\">overview</a>.</p>" // no comma here
        }
    ]
};
