# Local IspellDict: en
#+STARTUP: showeverything
#+SPDX-FileCopyrightText: 2017-2022,2024 Jens Lechtenbörger <https://lechten.gitlab.io/#me>
#+SPDX-License-Identifier: CC-BY-SA-4.0

#+INCLUDE: "config.org"
#+OPTIONS: toc:nil

#+TITLE: Virtual Memory with Linux
#+KEYWORDS: operating system, virtual memory, paging, Linux, proc, smem
#+DESCRIPTION: Demo for looking at memory usage with Linux: proc and smem

* Looking at Memory with Linux
(Specifics of Linux are not part of learning objectives.
However, the following illustrates shared memory,
and the pseudo-filesystem ~/proc~ will be revisited in other
presentations.)

** Linux Kernel: ~/proc/<pid>/~
   :PROPERTIES:
   :CUSTOM_ID: proc
   :END:
   #+INDEX: /proc!Filesystem (OS Memory Linux)
   - ~/proc~ is a pseudo-filesystem
     - See https://man7.org/linux/man-pages/man5/proc.5.html
       - (Specific to Linux kernel; incomplete or missing elsewhere)
     - “Pseudo”: Look and feel of any other filesystem
       - Subdirectories and files
       - However, files are no “real” files but meta-data
     - Interface to internal *kernel data structures*
       - One subdirectory per process ID
       - OS identifies process by integer number
       - Here and elsewhere, ~<pid>~ is meant as *placeholder* for such a number

*** Video about ~/proc~
    :PROPERTIES:
    :CUSTOM_ID: video-proc
    :END:
    @@html:<video controls width="1280" height="720" data-src="../../OS/videos/proc-smaps.mp4"></video>@@
    #+begin_notes
This video, “Looking at /proc” by
[[https://lechten.gitlab.io/#me][Jens Lechtenbörger]],
shares the presentation’s license terms, namely
[[https://creativecommons.org/licenses/by-sa/4.0/][CC BY-SA 4.0]].

The video shows some aspects of the ~/proc~ filesystem related to
memory management, which are described in more abstract form on
subsequent slides.
    #+end_notes

*** Drawing about ~/proc~
    :PROPERTIES:
    :CUSTOM_ID: drawing-proc
    :END:
    #+INDEX: /proc!Drawing (OS Memory Linux)
    {{{revealimg("./figures/external-non-free/jvns.ca/proc.meta",t,"60rh")}}}

*** Drawing about ~man~ pages
    {{{revealimg("./figures/external-non-free/jvns.ca/man.meta",t,"60rh")}}}

** Linux Kernel Memory Interface
   :PROPERTIES:
   :CUSTOM_ID: proc-memory
   :END:
   #+INDEX: /proc!Memory allocation (OS Memory Linux)
   - Memory allocation (and much more) visible under ~/proc/<pid>~
   - E.g.:
     - ~/proc/<pid>/pagemap~: One 64-bit value per virtual page
       - Mapping to RAM or swap area
     - ~/proc/<pid>/maps~: Mapped memory regions
     - ~/proc/<pid>/smaps~: Memory usage for mapped regions
   - Notice: Memory regions include *shared* libraries that are used by
     lots of processes

** GNU/Linux Reporting: ~smem~
   :PROPERTIES:
   :CUSTOM_ID: smem
   :END:
   #+INDEX: GNU/Linux!Tools!smem (OS Memory Linux)
   - User space tool to read ~smaps~ files: ~smem~
     - See https://linoxide.com/memory-usage-reporting-smem/
   - Terminology
     - *Virtual set size* (VSS): Size of virtual address space
     - *Resident set size* (RSS): Allocated main memory
       - Standard notion, yet overestimates memory usage as lots of
         memory is shared between processes
	 - Shared memory is added to the RSS of every sharing process
     - *Unique set size* (USS): memory allocated exclusively to process
       - That much would be returned upon process’ termination
     - *Proportional set size* (PSS): USS plus “fair share” of shared pages
       - If page shared by 5 processes, each gets a fifth of a page
         added to its PSS

*** Sample ~smem~ Output
#+BEGIN_SRC bash
$ smem -c "pid command uss pss rss vss" -P "bash|xinit|emacs"
  PID Command                          USS      PSS      RSS      VSS
  765 /usr/bin/xinit /etc/X11/Xse      220      285     2084    15952
 1390 /bin/bash -c libreoffice5.3      240      510     2936    13188
  826 /bin/bash /usr/bin/qubes-se      256      524     3008    13204
  750 -su -c /usr/bin/xinit /etc/      316      587     3368    21636
 1251 bash                            4864     5136     7900    26024
 2288 /usr/bin/python /usr/bin/sm     5272     6035     9432    24688
 1145 emacs                          90876    93224   106568   662768
#+END_SRC

*** Sample ~smem~ Graph
    {{{revealimg("./figures/screenshots/smem.meta","~smem --bar pid -c \"uss pss rss\" -P \"bash|xinit\"~","70rh")}}}

#+INCLUDE: "license-template.org"
