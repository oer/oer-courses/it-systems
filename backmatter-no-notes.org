# SPDX-FileCopyrightText: 2024 Jens Lechtenbörger
# SPDX-License-Identifier: CC0-1.0

** Bibliography
  :PROPERTIES:
  :reveal_data_state: no-toc-progress
  :HTML_HEADLINE_CLASS: no-toc-progress
  :CUSTOM_ID: bibliography
  :UNNUMBERED: t
  :END:

#+print_bibliography:

#+INCLUDE: "~/.emacs.d/oer-reveal-org/license-template.org"
