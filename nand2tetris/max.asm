// Write the larger of the two variables x and y to RAM[0].
// Idea: Compute y-x.  If result is >=0, then y is larger.
          @x    // Load location of x to A.  Replaced by @16 in CPU Emulator.
          D=M   // Load value of x to D.
          @y    // Load location of y to A.  Replaced by @17.
          D=M-D // Compute y-D (=y-x) to D.
          @Y_IS_LARGER // Load Jump target to A.  Replaced by @10.
          D;JGE        // If D is GE (greater or equal to 0), Jump.
                       // Otherwise, continue with next instruction.
          // If we are here, x is larger.
          // Load x to D and jump elsewhere for writing.
          @x
          D=M
          @STORE_RESULT // Load Jump target to A.  Replaced by @12.
          0;JMP         // Jump unconditionally.
(Y_IS_LARGER)   // Declare Jump target.
                // Next instruction (@y) is in ROM[10].
                // @Y_IS_LARGER is the address of that instruction.
                // If we are here, y is larger.  Copy via D to RAM[0].
          @y
          D=M
(STORE_RESULT)  // Declare Jump target.
                // Next two instructions write D to RAM[0].
          @0
          M=D
(END)           // Declare Jump target for end of program (ROM[14]).
                // By convention, infinite loops end programs in Hack.
          @END  // Load Jump target to A.
          0;JMP // Jump unconditionally to previous A-instruction.
