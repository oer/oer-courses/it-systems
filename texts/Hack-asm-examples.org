# Local IspellDict: en
#+SPDX-FileCopyrightText: 2024 Jens Lechtenbörger <https://lechten.gitlab.io/#me>
#+SPDX-License-Identifier: CC-BY-SA-4.0

#+LANGUAGE: en
#+STARTUP: showeverything

#+INCLUDE: "config.org"
#+OPTIONS: toc:nil

#+TITLE: Hack assembly fragments
#+DESCRIPTION: Examples for Hack assembly fragments.

* Hack assembly exercises

If you really want to understand this, please enter the instructions
in the CPU Emulator provided by Nand2Tetris.  Execute them and observe
their effects on registers and RAM locations.  (Maybe also effects on
the screen, to be explained later: Enter different numbers into
RAM[16384] and observe pixel changes towards the top left of the
screen...)


** First exercises

- Set A to 17

  #+begin_src text
  @17
  #+end_src

- Set D to A-1

  #+begin_src text
  D=A-1
  #+end_src

- Set both A and D to A + 1

  #+begin_src text
  AD=A+1
  #+end_src

- Set D to 19

  #+begin_src text
  @19
  D=A
  #+end_src

- Set RAM[5034] to D - 1

  #+begin_src text
  @5034
  M=D-1
  #+end_src

- Set RAM[53] to 171

  #+begin_src text
  @171
  D=A
  @53
  M=D
  #+end_src

- Load RAM[7], add 1, and store the result in D

  #+begin_src text
  @7
  D=M+1
  #+end_src

- Increment the number stored in the RAM location whose address is
  stored in RAM[42]

  #+begin_src text
  @42
  A=M
  M=M+1
  #+end_src

** Variables
If you write the code examples below to files (whose default endings
should be ~.asm~ for assembler code), variable names are resolved into
RAM locations by a software called assembler.

While we do not look into the inner working of the assembler, you can
load such ~asm~ files into the CPU Emulator, which also replaces
variable names (and other symbols) with appropriate numbers.  For
example, it will allocate variables to RAM locations starting with
RAM[16].  Note that you cannot enter variable names directly in the
CPU Emulator.

- sum = 0

  #+begin_src text
  @sum
  M=0
  #+end_src

- j = j + 1

  #+begin_src text
  @j
  M=M+1
  #+end_src

- q = sum + 12 - j

  #+begin_src text
  @sum
  D=M
  @12
  D=D+A
  @j
  D=D-M
  @q
  M=D
  #+end_src

#+INCLUDE: "license-template.org" :minlevel 1
