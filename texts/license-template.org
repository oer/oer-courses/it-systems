# SPDX-FileCopyrightText: 2017-2022 Jens Lechtenbörger
# SPDX-License-Identifier: CC0-1.0

# This file is meant for ordinary HTML export (instead of reveal.js).
# Then, oer-reveal includes license information in an HTML postamble,
# which is not sufficient if also PDF should be generated.
# During publication, use an empty string for :html-postamble
# (see elisp/publish.el).

* License Information
  :PROPERTIES:
  :reveal_data_state: no-toc-progress
  :HTML_HEADLINE_CLASS: no-toc-progress
  :CUSTOM_ID: license
  :UNNUMBERED: notoc
  :END:

{{{licensepreamble}}}

#+BEGIN_SRC emacs-lisp :results html :exports results
(oer-reveal-license-to-fmt 'html t nil t t t t)
#+END_SRC
#+BEGIN_SRC emacs-lisp :results latex :exports results
(oer-reveal-license-to-fmt 'pdf)
#+END_SRC
